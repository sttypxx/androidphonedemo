package com.usw.androidphonedemo.wifi;

import android.app.Dialog;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.gson.Gson;
import com.inuker.bluetooth.library.sdk.bean.WifiConfig;
import com.inuker.bluetooth.library.sdk.bean.WifiConfigCallback;
import com.usw.androidphonedemo.R;
import com.usw.androidphonedemo.activity.DeviceControlActivity;

/**
 *
 */
public class WifiBTConnectDialogFragment extends DialogFragment implements View.OnClickListener {

    private static final String TAG = WifiBTConnectDialogFragment.class.getSimpleName();

    private TextView        mSsidView;
    private EditText        mPasswordEditView;
    private TextView        mCancelBtn;
    private TextView        mDisconnectBtn;
    private TextView        mHeadInfoView;

    private WifiManager     mWifiManger;
    private WifiInfoBean    wifiInfoBean;

    private Handler mHandler = new Handler(Looper.getMainLooper());
    private Context mContext;

    /**
     *  WifiBTConnectDialogFragment
     *  @param wifiManager
     *  @param wifiInfoBean
     */
    public WifiBTConnectDialogFragment(WifiManager wifiManager, WifiInfoBean wifiInfoBean) {

        this.mWifiManger = wifiManager;
        this.wifiInfoBean = wifiInfoBean;
    }

    /**
     *  onCreate
     *  @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        mContext = getContext();
    }

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_fragment_wifi_connect_bt, container, false);

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        mSsidView       =   view.findViewById(R.id.id_wifi_connect_ssid_view_bt);
        mPasswordEditView = view.findViewById(R.id.id_wifi_password_edit_view_bt);
        mCancelBtn      =   view.findViewById(R.id.id_wifi_cancel_btn_bt);
        mDisconnectBtn  =   view.findViewById(R.id.id_wifi_disconnect_btn_bt);
        mHeadInfoView   =   view.findViewById(R.id.id_wifi_head_info_view_bt);

        mCancelBtn.setOnClickListener(this);
        mDisconnectBtn.setOnClickListener(this);

        mSsidView.setText(String.format("SSID: %s", wifiInfoBean.getSsid()));

//        if  (wifiInfoBean.isConnectState()) {
//
//            mHeadInfoView.setText(R.string.wifi_connect_head_info);
//            mPasswordEditView.setEnabled(false);
//            mPasswordEditView.setHint("");
//            mDisconnectBtn.setTag("disconnect");
//            mDisconnectBtn.setText(R.string.btn_wifi_disconnect);
//
//        } else {

            mHeadInfoView.setText(R.string.wifi_input_password_head_info);
            mPasswordEditView.setEnabled(true);
            mDisconnectBtn.setTag("connect");
            mPasswordEditView.setHint(R.string.wifi_password);
            mDisconnectBtn.setText(R.string.btn_wifi_connect);
//        }

        if  (WifiSupport.getWifiCipher(wifiInfoBean.getCapabilities()) == WifiSupport.WifiCipherType.WIFICIPHER_NOPASS) {

            mPasswordEditView.setEnabled(false);
        }

        return  view;
    }

    DeviceControlActivity   deviceControlActivity;

    /**
     *
     * @param deviceControlActivity
     */
    public void setDeviceControlActivity(DeviceControlActivity deviceControlActivity)
    {
        this.deviceControlActivity = deviceControlActivity;
    }

    /**
     *
     */
    @Override
    public void onStart() {

        super.onStart();

        try {

            Dialog dialog = getDialog();
            if (dialog != null) {

                DisplayMetrics dm = new DisplayMetrics();
                getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
                dialog.getWindow().setLayout((int) (dm.widthPixels * 0.6), (int) (dm.heightPixels * 0.5));

                dialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_dialog));
            }

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    /**
     *
     */
    @Override
    public void onStop() {

        super.onStop();

        wifiInfoBean = null;
        mWifiManger = null;
    }

    /**
     *  onDestroy
     */
    @Override
    public void onDestroy() {

        super.onDestroy();
    }

    /**
     *  onClick
     *  @param v
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.id_wifi_connect_btn_bt:
                break;

            case R.id.id_wifi_disconnect_btn_bt:

                if  ("connect".equals(v.getTag())) {

                    if  (getSecurity(wifiInfoBean) == WIFI_NO_PASS) {

                        connectNoPasswordWifiViaBT();
                    } else {

                        String password = mPasswordEditView.getText().toString();
                        connectPasswordWifiViaBT(password);
                    }
                }
                else
                {
                    if  (getSecurity(wifiInfoBean) == WIFI_NO_PASS) {

                        connectNoPasswordWifiViaBT();
                    } else {

                        String password = mPasswordEditView.getText().toString();
                        connectPasswordWifiViaBT(password);
                    }
                }
                break;

            case R.id.id_wifi_cancel_btn_bt:

                dismiss();
                break;

            default:
                break;
        }
    }

    /**
     *  connectNoPasswordWifiViaBT
     *  @param
     */
    private void connectNoPasswordWifiViaBT()
    {
        Gson        gson;
        WifiConfig  wifiConfig;
        String      strWifiConfig;

        wifiConfig = new WifiConfig();

        wifiConfig.setSsid(wifiInfoBean.getSsid());
        wifiConfig.setPassword("");
        wifiConfig.setType(getWifiCipher(wifiInfoBean.getCapabilities()));

        gson = new Gson();
        strWifiConfig = gson.toJson(wifiConfig);

        Toast.makeText(getActivity(), "connectNoPasswordWifiViaBT(), WifiConfig = " + strWifiConfig, Toast.LENGTH_LONG).show();

        if  (deviceControlActivity != null)
        {
            deviceControlActivity.writeNewWifiConfig(wifiConfig, new WifiConfigCallback() {

                @Override
                public void onReadCallback(int code, WifiConfig config) {

                }

                @Override
                public void onWriteCallback(int code) {

                }
            });
        }
        else
        {
            Log.i(TAG, "deviceControlActivity is NULL");
        }

        mHandler.postDelayed(new Runnable() {

            @Override
            public void run() {

                dismiss();
            }
        }, 5000);
    }

    /**
     *  connectPasswordWifi
     *  @param password
     */
    private void connectPasswordWifiViaBT(String password)
    {
        Gson        gson;
        WifiConfig  wifiConfig;
        String      strWifiConfig;

        wifiConfig = new WifiConfig();

        wifiConfig.setSsid(wifiInfoBean.getSsid());
        wifiConfig.setPassword(password);
        wifiConfig.setType(getWifiCipher(wifiInfoBean.getCapabilities()));

        gson = new Gson();
        strWifiConfig = gson.toJson(wifiConfig);

        Toast.makeText(getActivity(), "connectPasswordWifiViaBT(), WifiConfig = " + strWifiConfig, Toast.LENGTH_LONG).show();

        if  (deviceControlActivity != null)
        {
            deviceControlActivity.writeNewWifiConfig(wifiConfig, new WifiConfigCallback() {

                @Override
                public void onReadCallback(int code, WifiConfig config) {

                }

                @Override
                public void onWriteCallback(int code) {

                }
            });
        }
        else
        {
            Log.i(TAG, "deviceControlActivity is NULL");
        }

        mHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                dismiss();
            }
        }, 5000);
    }

    /**
     * 判断wifi热点支持的加密方式
     */
    public static String getWifiCipher(String s) {

        if  (s.isEmpty()) {

            return "INVALID";

        } else if (s.contains("WEP")) {

            return  "WEP";
        } else if (s.contains("WPA") || s.contains("WPA2") || s.contains("WPS")) {

            return  "WPA";
        } else {

            return  "NOPASS";
        }
    }

    public static final int WIFI_NO_PASS = 0;
    private static final int WIFI_WEP = 1;
    private static final int WIFI_PSK = 2;
    private static final int WIFI_EAP = 3;

    /**
     * 判断是否有密码.
     *
     * @param result ScanResult
     * @return 0
     */
    public static int getSecurity(WifiInfoBean result) {

        if  (null != result && null != result.getCapabilities()) {

            if  (result.getCapabilities().contains("WEP")) {

                return WIFI_WEP;
            } else if (result.getCapabilities().contains("PSK")) {

                return WIFI_PSK;
            } else if (result.getCapabilities().contains("EAP")) {

                return WIFI_EAP;
            }
        }

        return  WIFI_NO_PASS;
    }
}
