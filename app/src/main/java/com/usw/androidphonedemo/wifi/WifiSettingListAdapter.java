package com.usw.androidphonedemo.wifi;
/*
 *
 * Author: xj.luo
 * Email: xj_luo@foxmail.com
 * Time: Created on 2020/3/25
 *
 */

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.usw.androidphonedemo.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 */
public class WifiSettingListAdapter extends RecyclerView.Adapter<WifiSettingListAdapter.WifiSettingViewHolder> {

    private static final String TAG = WifiSettingListAdapter.class.getSimpleName();

    private Context             mContext;
    private List<WifiInfoBean>  list = new ArrayList<>();
    private ItemClickListener   itemClickListener;

    public interface ItemClickListener {

        void onClick(WifiInfoBean wifiInfoBean);
    }

    public WifiSettingListAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {

        this.itemClickListener = itemClickListener;
    }

    /**
     *
     * @param s
     * @param connectInfo
     */
    public void setScanResults(List<ScanResult> s, WifiInfo connectInfo) {

        list.clear();

        if  (s != null && s.size() > 0) {

            for (int i = 0; i < s.size(); i++) {

                WifiInfoBean wifiInfoBean = new WifiInfoBean();

                wifiInfoBean.setSsid(s.get(i).SSID);
                wifiInfoBean.setBssid(s.get(i).BSSID);
                wifiInfoBean.setCapabilities(s.get(i).capabilities);

                Log.i(TAG, "wifi info bean ssid "   +   wifiInfoBean.getSsid());
                Log.i(TAG, "connect info ssid is "  +   connectInfo.getSSID());
                Log.i(TAG, "connect info bssid is " +   connectInfo.getBSSID());

                if (connectInfo != null && wifiInfoBean.getBssid().equals(connectInfo.getBSSID())) {

                    Log.i(TAG, "wifi info set connect state is true");
                    wifiInfoBean.setConnectState(true);

                } else {

                    Log.i(TAG, "wifi info set connect state is false");
                    wifiInfoBean.setConnectState(false);
                }

                wifiInfoBean.setSignalQuality(s.get(i).level + " dBm");

                list.add(wifiInfoBean);
            }

            //  排序，如果已连接，排在最前面
            Collections.sort(list);
        }

        notifyDataSetChanged();
    }

    /**
     *
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public WifiSettingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.item_wifi_list_recycle, parent, false);

        return new WifiSettingViewHolder(view);
    }

    /**
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull WifiSettingViewHolder holder, int position) {

        if  (list != null && list.size() > 0) {

            final WifiInfoBean wifiInfoBean = list.get(position);

            if  (wifiInfoBean.isConnectState()) {

                holder.ssidView.setText(wifiInfoBean.getSsid());
                holder.ssidView.setTextColor(mContext.getResources().getColor(R.color.color_connect));
                holder.connectStateView.setText(mContext.getResources().getString(R.string.wifi_connect));

            } else {

                holder.ssidView.setText(wifiInfoBean.getSsid());
                //holder.ssidView.setTextColor(Color.WHITE);
                holder.connectStateView.setText(mContext.getResources().getString(R.string.wifi_not_connect));
            }

            holder.signalQualityView.setText(list.get(position).getSignalQuality());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if  (itemClickListener != null) {
                        itemClickListener.onClick(wifiInfoBean);
                    }
                }
            });
        }
    }

    /**
     *
     * @return
     */
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    /**
     *
     */
    public static class WifiSettingViewHolder extends RecyclerView.ViewHolder {

        TextView ssidView;
        TextView connectStateView;
        TextView signalQualityView;

        public WifiSettingViewHolder(View itemView) {

            super(itemView);

            ssidView = itemView.findViewById(R.id.id_wifi_item_ssid);
            connectStateView = itemView.findViewById(R.id.id_wifi_item_connect_state);
            signalQualityView = itemView.findViewById(R.id.id_wifi_item_signal_quality);
        }
    }
}
