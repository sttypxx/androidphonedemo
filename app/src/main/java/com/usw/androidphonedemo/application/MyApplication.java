package com.usw.androidphonedemo.application;

import android.app.Application;
import android.util.Log;

public class MyApplication extends Application  {

    private static final String TAG = MyApplication.class.getSimpleName();

    private static MyApplication mInstance = null;

    /**
     *
     */
    @Override
    public void onCreate() {

        super.onCreate();

        mInstance = this;
    }

    /**
     *
     * @return
     */
    public static MyApplication getInstance()
    {
        return  mInstance;
    }
}
