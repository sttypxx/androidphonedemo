package com.usw.androidphonedemo.util;
/*
 * Author: xj.luo
 * Email: xj_luo@foxmail.com
 * Time: Created on 2020/4/21
 *
 */


import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.usw.androidphonedemo.R;

import java.lang.ref.WeakReference;

public class ToastCustom extends Toast {
    private static Toast mToast;

    public ToastCustom(Context context) {
        super(context);
    }

    public static void showToast(Context context, String content) {

        //获取系统的LayoutInflater
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.toast_layout, null);

        TextView tv_content = view.findViewById(R.id.tv_content);
        tv_content.setText(content);

        //实例化toast
        mToast = new Toast(context);
        mToast.setView(view);
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.setGravity(Gravity.BOTTOM, 0, 10);
        mToast.show();
    }

    public static void showToast(Context context, String content, int gravity) {

        //获取系统的LayoutInflater
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.toast_layout, null);

        TextView tv_content = view.findViewById(R.id.tv_content);
        tv_content.setText(content);

        //  实例化toast
        mToast = new Toast(context);
        mToast.setView(view);
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.setGravity(gravity, 0, 10);
        mToast.show();
    }

    // 测试可用，能取消之前的消息，显示最新的消息
    public synchronized static void syncOnesShowToast(Context context, String content) {
        WeakReference<Context> contextWeakReference = new WeakReference<>(context);
        //获取系统的LayoutInflater
        LayoutInflater inflater = (LayoutInflater) contextWeakReference.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.toast_layout, null);

        TextView tv_content = view.findViewById(R.id.tv_content);
        tv_content.setText(content);

        //实例化toast
        if (mToast == null) {
            mToast = new Toast(contextWeakReference.get());
        }

        mToast.setView(view);
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.setGravity(Gravity.BOTTOM, 0, 10);
        mToast.show();

    }

}
