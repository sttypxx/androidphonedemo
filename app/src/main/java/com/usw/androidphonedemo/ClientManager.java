package com.usw.androidphonedemo;

import com.inuker.bluetooth.library.BluetoothClient;
import com.usw.androidphonedemo.application.MyApplication;

/**
 * Created by dingjikerbo on 2016/8/27.
 */
public class ClientManager {

    private static BluetoothClient mClient;

    /**
     *
     * @return
     */
    public static BluetoothClient getClient() {

        if  (mClient == null) {

            synchronized (ClientManager.class) {

                if  (mClient == null) {

                    mClient = new BluetoothClient(MyApplication.getInstance());
                }
            }
        }

        return  mClient;
    }
}
