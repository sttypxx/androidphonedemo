package com.usw.androidphonedemo.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.inuker.bluetooth.library.connect.listener.BluetoothStateListener;
import com.inuker.bluetooth.library.search.SearchRequest;
import com.inuker.bluetooth.library.search.SearchResult;
import com.inuker.bluetooth.library.search.response.SearchResponse;
import com.inuker.bluetooth.library.utils.BluetoothLog;
import com.usw.androidphonedemo.AppConstants;
import com.usw.androidphonedemo.ClientManager;
import com.usw.androidphonedemo.R;
import com.usw.androidphonedemo.view.DeviceListAdapter;
import com.usw.androidphonedemo.view.PullRefreshListView;
import com.usw.androidphonedemo.view.PullToRefreshFrameLayout;

import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 *
 */
public class DeviceScanActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = DeviceScanActivity.class.getSimpleName();

    public static final String EXTRAS_DEVICE_NAME       =   "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS    =   "DEVICE_ADDRESS";

    private PullToRefreshFrameLayout    mRefreshLayout;
    private PullRefreshListView         mListView;
    private DeviceListAdapter           mAdapter;
    private TextView                    mTvTitle;

    private String                      mDeviceName;
    private String                      mDeviceAddress;

    private List<SearchResult>          mDevices;

    private static final int PERMISSIONS_ID             =   1000;

    private String[] permissions = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION};

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_scan);

        final Intent intent = getIntent();
        mDeviceName     =   intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress  =   intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        mDevices = new ArrayList<SearchResult>();

        mTvTitle = (TextView) findViewById(R.id.title);

        mRefreshLayout = (PullToRefreshFrameLayout) findViewById(R.id.pulllayout);

        mListView = mRefreshLayout.getPullToRefreshListView();
        mAdapter = new DeviceListAdapter(this);
        mListView.setAdapter(mAdapter);

        mListView.setOnRefreshListener(new PullRefreshListView.OnRefreshListener() {

            @Override
            public void onRefresh() {

                searchDevice();
            }

        });
    }

    @Override
    public void onClick(View v) {

    }

    /**
     *
     */
    @Override
    protected void onStart() {

        super.onStart();

        initAll();
    }

    /**
     *
     */
    @AfterPermissionGranted(PERMISSIONS_ID)
    private void methodRequiresTwoPermission() {

        initAll();
    }

    /**
     *
     */
    private void initAll() {

        if  (EasyPermissions.hasPermissions(this, permissions)) {

            //  init
            init();

        } else {

            EasyPermissions.requestPermissions(this, getString(R.string.permission_string), PERMISSIONS_ID, permissions);
        }
    }

    private void init()
    {
        searchDevice();

        ClientManager.getClient().registerBluetoothStateListener(new BluetoothStateListener() {
            @Override
            public void onBluetoothStateChanged(boolean openOrClosed) {
                BluetoothLog.v(String.format("onBluetoothStateChanged %b", openOrClosed));
            }
        });
    }

    /**
     *
     */
    private void searchDevice() {

        SearchRequest request = new SearchRequest.Builder()
                .searchBluetoothLeDevice(5000, 2).build();

        ClientManager.getClient().search(request, mSearchResponse);
    }

    private final SearchResponse mSearchResponse = new SearchResponse() {

        @Override
        public void onSearchStarted() {

            BluetoothLog.w("MainActivity.onSearchStarted");

            mListView.onRefreshComplete(true);
            mRefreshLayout.showState(AppConstants.LIST);
            mTvTitle.setText(R.string.string_refreshing);
            mDevices.clear();
        }

        @Override
        public void onDeviceFounded(SearchResult device) {

            String  foundDeviceName;

//            BluetoothLog.w("MainActivity.onDeviceFounded " + device.device.getAddress());

            if  (!mDevices.contains(device)) {

                foundDeviceName = device.getName();

                if  (IsOurTargetDevice(foundDeviceName, mDeviceName)) {

                    mDevices.add(device);
                    mAdapter.setDataList(mDevices);

                    BluetoothLog.w("DeviceScanActivity.onDeviceFounded " + device.getName());
                }
            }

            if  (mDevices.size() > 0) {

                mRefreshLayout.showState(AppConstants.LIST);
            }
        }

        @Override
        public void onSearchStopped() {

            BluetoothLog.w("MainActivity.onSearchStopped");

            mListView.onRefreshComplete(true);
            mRefreshLayout.showState(AppConstants.LIST);

            mTvTitle.setText(R.string.devices);
        }

        @Override
        public void onSearchCanceled() {

            BluetoothLog.w("MainActivity.onSearchCanceled");

            mListView.onRefreshComplete(true);
            mRefreshLayout.showState(AppConstants.LIST);

            mTvTitle.setText(R.string.devices);
        }
    };

    /**
     *
     * @param mac
     * @return
     */
    private String MacFormat(String mac){

        if  (TextUtils.isEmpty(mac))  {

            return  null;
        } else if (mac.contains(":")) {

            mac = mac.replace(":", "");
        }

        return  mac.toUpperCase();
    }

    /**
     *
     * @param deviceName
     * @return
     */
    private boolean IsOurTargetDevice(String deviceName, String targetDeviceName)
    {
        String[]    deviceNameArray;
        String      foundMac;
        String      targetMac;

        Log.i(TAG, "IsOurTarget(), deviceName = " + deviceName);

        targetMac       =   MacFormat(targetDeviceName);
        deviceNameArray =   deviceName.split("_");

        if  (deviceName.contains("TF")) {

            Log.i(TAG, "Got our Target device");
        }

        if  (deviceNameArray.length < 3)
            return  false;

        foundMac        =   deviceNameArray[2];

        if (foundMac.equals(targetMac))
        {
            return  true;
        }

        return  false;
    }

    /**
     *
     */
    @Override
    protected void onPause() {

        super.onPause();

        ClientManager.getClient().stopSearch();
    }
}
