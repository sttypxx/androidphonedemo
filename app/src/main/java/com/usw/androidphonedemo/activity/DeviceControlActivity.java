/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.usw.androidphonedemo.activity;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.inuker.bluetooth.library.sdk.bean.WifiConfig;
import com.inuker.bluetooth.library.sdk.bean.WifiConfigCallback;
import com.inuker.bluetooth.library.sdk.bluetoothgatt.BluetoothLeService;
import com.inuker.bluetooth.library.sdk.bluetoothgatt.SampleGattAttributes;
import com.usw.androidphonedemo.R;
import com.usw.androidphonedemo.wifi.SettingNetworkFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * For a given BLE device, this Activity provides the user interface to connect, display data,
 * and display GATT services and characteristics supported by the device.  The Activity
 * communicates with {@code BluetoothLeService}, which in turn interacts with the
 * Bluetooth LE API.
 */
public class DeviceControlActivity extends AppCompatActivity
        implements View.OnClickListener, WifiConfigCallback
{
    private final static String TAG = DeviceControlActivity.class.getSimpleName();

    public static final String EXTRAS_DEVICE_NAME       =   "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS    =   "DEVICE_ADDRESS";

    private TextView                mConnectionState;
    private TextView                mDataField;
    private String                  mDeviceName;
    private String                  mDeviceAddress;
//    private ExpandableListView      mGattServicesList;
    private BluetoothLeService      mBluetoothLeService;

    private int wifiStartScanAction = 1011;
    private boolean isConnecting = false;

    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

    private boolean                 mConnected = false;
    private BluetoothGattCharacteristic     mNotifyCharacteristic;

    private final String    LIST_NAME = "NAME";
    private final String    LIST_UUID = "UUID";

    private Button          btn_getConfig;
    private Button          btn_setConfig;
    private Button          btn_scan;

    private EditText        tv_ssid;
    private EditText        tv_password;

    public static final int SHOW_TEXT = 1001;    //接收字幕数据

    private Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {

            switch (msg.what) {

                case SHOW_TEXT:
                    //  接收服务器发送过来的字幕数据
                    String msgContent = (String) msg.obj;
                    showText(msgContent);
                    break;
            }

            return  false;
        }
    });

    //  Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {

            Log.i(TAG, "onServiceConnected, ComponentName = " + componentName.getPackageName());

            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if  (!mBluetoothLeService.initialize()) {

                Log.e(TAG, "Unable to initialize Bluetooth");

                finish();
            }

            //  Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

            Log.i(TAG, "onServiceDisconnected, ComponentName = " + componentName.getPackageName());

            mBluetoothLeService = null;
        }
    };

    //  Handles various events fired by the Service.
    //  ACTION_GATT_CONNECTED: connected to a GATT server.
    //  ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    //  ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    //  ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            final String action = intent.getAction();

            if  (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {

                mConnected = true;
                updateConnectionState(R.string.connected);
                invalidateOptionsMenu();

            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {

                mConnected = false;
                updateConnectionState(R.string.disconnected);
                invalidateOptionsMenu();
                clearUI();

            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {

                //  Show all the supported services and characteristics on the user interface.
//                displayGattServices(mBluetoothLeService.getSupportedGattServices());

            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {

                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            }
        }
    };

    // If a given GATT characteristic is selected, check for supported features.  This sample
    // demonstrates 'Read' and 'Notify' features.  See
    // http://d.android.com/reference/android/bluetooth/BluetoothGatt.html for the complete
    // list of supported characteristic features.
//    private final ExpandableListView.OnChildClickListener servicesListClickListner =
//            new ExpandableListView.OnChildClickListener() {
//                @Override
//                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
//                                            int childPosition, long id) {
//
//                    if  (mGattCharacteristics != null) {
//
//                        final BluetoothGattCharacteristic characteristic =
//                                mGattCharacteristics.get(groupPosition).get(childPosition);
//                        final int charaProp = characteristic.getProperties();
//                        if  ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
//
//                            //  If there is an active notification on a characteristic, clear
//                            //  it first so it doesn't update the data field on the user interface.
//                            if  (mNotifyCharacteristic != null) {
//
//                                mBluetoothLeService.setCharacteristicNotification(
//                                        mNotifyCharacteristic, false);
//                                mNotifyCharacteristic = null;
//                            }
//
//                            mBluetoothLeService.readCharacteristic(characteristic);
//                        }
//
//                        if  ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
//
//                            mNotifyCharacteristic = characteristic;
//                            mBluetoothLeService.setCharacteristicNotification(
//                                    characteristic, true);
//                        }
//
//                        return true;
//                    }
//
//                    return false;
//                }
//    };

    /**
     *
     */
    private void clearUI() {

//        mGattServicesList.setAdapter((SimpleExpandableListAdapter) null);
        mDataField.setText(R.string.no_data);
    }

    /**
     *  onCreate
     *  @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.gatt_services_characteristics);

        final Intent intent = getIntent();
        mDeviceName     =   intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress  =   intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        //  Sets up UI references.
        ((TextView) findViewById(R.id.device_address)).setText(mDeviceAddress);

//        mGattServicesList   =   (ExpandableListView) findViewById(R.id.gatt_services_list);
//        mGattServicesList.setOnChildClickListener(servicesListClickListner);

        mConnectionState    =   (TextView) findViewById(R.id.connection_state);
        mDataField = (TextView) findViewById(R.id.data_value);

        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        btn_getConfig   =   findViewById(R.id.btn_getConfig);
        btn_getConfig.setOnClickListener(this);

        btn_scan = findViewById(R.id.btn_scan);
        btn_scan.setOnClickListener(this);

//        btn_setConfig   =   findViewById(R.id.btn_setConfig);
//        btn_setConfig.setOnClickListener(this);

//        tv_ssid         =   (EditText)findViewById(R.id.tv_ssid);
//        tv_password     =   (EditText)findViewById(R.id.tv_password);

        loadMixFragment();
    }

    /**
     *  onClick
     *  @param v
     */
    @Override
    public void onClick(View v) {

        switch(v.getId())
        {
            case R.id.btn_getConfig:

                readCurrentWifiConfig(this);
                break;

            case R.id.btn_scan:
                break;

//            case R.id.btn_setConfig:
//                String      ssid;
//                String      password;
//                WifiConfig wifiConfig;
//
//                ssid = tv_ssid.getText().toString();
//                password = tv_password.getText().toString();
//
//                if  (ssid.isEmpty()) {
//
//                    Toast.makeText(DeviceControlActivity.this, "SSID can not be empty", Toast.LENGTH_SHORT).show();
//                    break;
//                }
//
//                wifiConfig = new WifiConfig();
//                wifiConfig.setSsid(ssid);
//                wifiConfig.setPassword(password);
//
//                writeNewWifiConfig(wifiConfig, this);
//                break;
        }
    }

    @Override
    protected void onStart() {

        super.onStart();

    }

    /**
     *  load Mix Fragment for photo and video slide show.
     */
    private void loadMixFragment() {

        try
        {
            FragmentManager mFragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

            SettingNetworkFragment settingNetworkFragment = new SettingNetworkFragment();

            settingNetworkFragment.setDeviceControlActivity(this);

            fragmentTransaction.replace(R.id.id_root_frame_layout, settingNetworkFragment, SettingNetworkFragment.class.getSimpleName());
            fragmentTransaction.commitNowAllowingStateLoss();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     *  onResume
     */
    @Override
    protected void onResume() {

        super.onResume();

        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if  (mBluetoothLeService != null) {

            final boolean result = mBluetoothLeService.connect(mDeviceAddress);

            Log.d(TAG, "Connect request result = " + result);
        }
    }

    /**
     *  onPause
     */
    @Override
    protected void onPause() {

        super.onPause();

        unregisterReceiver(mGattUpdateReceiver);
    }

    /**
     *
     */
    @Override
    protected void onDestroy() {

        super.onDestroy();

        unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

    /**
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

//        getMenuInflater().inflate(R.menu.gatt_services, menu);
//
//        if  (mConnected) {
//
//            menu.findItem(R.id.menu_connect).setVisible(false);
//            menu.findItem(R.id.menu_disconnect).setVisible(true);
//
//        } else {
//
//            menu.findItem(R.id.menu_connect).setVisible(true);
//            menu.findItem(R.id.menu_disconnect).setVisible(false);
//        }

        return  true;
    }

    /**
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        switch(item.getItemId()) {
//
//            case R.id.menu_connect:
//
//                mBluetoothLeService.connect(mDeviceAddress);
//                return  true;
//
//            case R.id.menu_disconnect:
//
//                mBluetoothLeService.disconnect();
//                return  true;
//
//            case android.R.id.home:
//
//                onBackPressed();
//                return true;
//        }

        return  super.onOptionsItemSelected(item);
    }

    /**
     *
     * @param resourceId
     */
    private void updateConnectionState(final int resourceId) {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                mConnectionState.setText(resourceId);
            }
        });
    }

    /**
     *
     * @param data
     */
    private void displayData(String data) {

        if  (data != null) {

            mDataField.setText(data);
        }
    }

    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
    // In this sample, we populate the data structure that is bound to the ExpandableListView
    // on the UI.
    private void displayGattServices(List<BluetoothGattService> gattServices) {

        if  (gattServices == null) return;

        String  uuid = null;
        String  unknownServiceString = getResources().getString(R.string.unknown_service);
        String  unknownCharaString = getResources().getString(R.string.unknown_characteristic);

        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
                = new ArrayList<ArrayList<HashMap<String, String>>>();
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        //  Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {

            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();

            if  (!SampleGattAttributes.containsUuid(uuid))
            {
                //  Do not display unknown common service
                continue;
            }

            currentServiceData.put(
                    LIST_NAME, SampleGattAttributes.lookup(uuid, unknownServiceString));
            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                    new ArrayList<HashMap<String, String>>();

            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();

            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();

            //  Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {

                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();
                currentCharaData.put(
                        LIST_NAME, SampleGattAttributes.lookup(uuid, unknownCharaString));

                currentCharaData.put(LIST_UUID, uuid);
                gattCharacteristicGroupData.add(currentCharaData);
            }

            mGattCharacteristics.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }

        SimpleExpandableListAdapter gattServiceAdapter = new SimpleExpandableListAdapter(
                this,
                gattServiceData,
                android.R.layout.simple_expandable_list_item_2,
                new String[] {LIST_NAME, LIST_UUID},
                new int[] { android.R.id.text1, android.R.id.text2 },
                gattCharacteristicData,
                android.R.layout.simple_expandable_list_item_2,
                new String[] {LIST_NAME, LIST_UUID},
                new int[] { android.R.id.text1, android.R.id.text2 }
        );

//        mGattServicesList.setAdapter(gattServiceAdapter);
    }

    /**
     *
     * @return
     */
    private static IntentFilter makeGattUpdateIntentFilter() {

        final IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);

        return  intentFilter;
    }

    /**
     *  read Current Wifi Config
     *  @param callback
     *  @return
     */
    public void readCurrentWifiConfig(WifiConfigCallback callback)
    {
        mBluetoothLeService.readCurrentWifiConfig(callback);
    }

    /**
     *  writeNewWifiConfig
     *  @param wifiConfig
     *  @param callback
     *  @return
     */
    public void writeNewWifiConfig(WifiConfig wifiConfig, WifiConfigCallback callback)
    {
        Log.i(TAG, "Call BluetoothLeService.writeConfig to write wifi configuration");
        Log.i(TAG, "wifiConfig = " + wifiConfig.toString());

        mBluetoothLeService.writeWifiConfig(wifiConfig, callback);
    }

    /**
     *  onReadCallback
     *  @param code
     *  @param config
     */
    @Override
    public void onReadCallback(int code, WifiConfig config) {

        if  (0 == code) {

            Message     message;
            message = new Message();
            message.what = SHOW_TEXT;
            message.obj  = "onReadCallback, (" + config.getSsid() + "," + config.getPassword() + ")";

            mHandler.sendMessageDelayed(message, 0);

            if  (config != null) {

                Log.i(TAG, "onReadCallback," + "code = " + code + ",WifiConfig(" + config.getSsid() + "," + config.getPassword() + ")");
            }
            else {

                Log.i(TAG, "onReadCallback," + "code = " + code + ",WifiConfig is NULL");
            }
        }
    }

    /**
     *  onWriteCallback
     *  @param code
     */
    @Override
    public void onWriteCallback(int code) {

        if  (code == 0) {

            Message     message;
            message = new Message();
            message.what = SHOW_TEXT;
            message.obj  = "onWriteCallback, code = " + code;

            mHandler.sendMessageDelayed(message, 0);

            Log.i(TAG, "onWriteCallback, code = " + code);
        }
    }

    /**
     *
     * @param msg
     */
    private void showText(final String msg) {

        Toast.makeText(DeviceControlActivity.this, msg, Toast.LENGTH_LONG).show();
    }
}
