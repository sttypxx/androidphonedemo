package com.usw.androidphonedemo.activity;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.usw.androidphonedemo.R;
import com.usw.androidphonedemo.zxing.android.CaptureActivity;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String DECODED_CONTENT_KEY =   "codedContent";
    private static final String DECODED_BITMAP_KEY  =   "codedBitmap";

    private static final int REQUEST_CODE_SCAN      =   0x0000;
    private static final int PERMISSIONS_ID = 1000;

    private Button      btn_scan;
    private Button      btn_connect;
    private TextView    tv_scanResult;
    private String      targetBluetoothMac;

    /*  Bluetooth API */
    private BluetoothManager mBluetoothManager;

    private String[] permissions = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION};

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_scanResult = (TextView) findViewById(R.id.tv_scanResult);

        //  Scan
        btn_scan = (Button) findViewById(R.id.btn_scan);
        btn_scan.setOnClickListener(this);

        //  Connect
        btn_connect = (Button)findViewById(R.id.btn_connect);
        btn_connect.setOnClickListener(this);
    }

    /**
     *
     */
    @Override
    protected void onStart() {

        super.onStart();

        initAll();
    }

    /**
     * Verify the level of Bluetooth support provided by the hardware.
     * @param bluetoothAdapter System {@link BluetoothAdapter}.
     * @return true if Bluetooth is properly supported, false otherwise.
     */
    private boolean checkBluetoothSupport(BluetoothAdapter bluetoothAdapter) {

        if  (bluetoothAdapter == null) {

            Log.w(TAG, "Bluetooth is not supported");

            return  false;
        }

        if  (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {

            Log.w(TAG, "Bluetooth LE is not supported");

            return  false;
        }

        return  true;
    }

    /**
     *
     */
    private void handleBluetooth()
    {
        mBluetoothManager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        BluetoothAdapter bluetoothAdapter = mBluetoothManager.getAdapter();

        //  We can't continue without proper Bluetooth support
        if  (!checkBluetoothSupport(bluetoothAdapter)) {

            Toast.makeText(MainActivity.this, "Bluetooth does not support in this machine", Toast.LENGTH_LONG).show();

            finish();
        }

        if  (!bluetoothAdapter.isEnabled()) {

            Log.d(TAG, "Bluetooth is currently disabled...enabling");

            bluetoothAdapter.enable();

        } else {

            Log.d(TAG, "Bluetooth enabled...starting services");
        }
    }

    /**
     *
     */
    @AfterPermissionGranted(PERMISSIONS_ID)
    private void methodRequiresTwoPermission() {

        initAll();
    }

    /**
     *
     */
    private void initAll() {

        if  (EasyPermissions.hasPermissions(this, permissions)) {

            //  Handle all bluetooth processing
            handleBluetooth();

        } else {

            EasyPermissions.requestPermissions(this, getString(R.string.permission_string), PERMISSIONS_ID, permissions);
        }
    }

    /**
     *
     * @param v
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case    R.id.btn_scan:

                //  request permission for camera
                if  (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, 1);

                } else {

                    goScan();
                }
                break;

            case    R.id.btn_connect:

                connectTo(targetBluetoothMac);
                break;

            default:
                break;
        }
    }

    /**
     *
     * @param bluetoothMacAddr
     */
    private void connectTo(String bluetoothMacAddr)
    {

        final Intent intent = new Intent(this, DeviceScanActivity.class);

        intent.putExtra(DeviceScanActivity.EXTRAS_DEVICE_NAME,       bluetoothMacAddr);
        intent.putExtra(DeviceScanActivity.EXTRAS_DEVICE_ADDRESS,    bluetoothMacAddr);

        startActivity(intent);

        Log.i(TAG, "connectTo(), bluetoothMacAddr = " + bluetoothMacAddr);
    }

    /**
     * Jump to scan
     */
    private void goScan()
    {
        Intent intent = new Intent(MainActivity.this, CaptureActivity.class);
        startActivityForResult(intent, REQUEST_CODE_SCAN);
    }

    /**
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        //  scan code return
        if  (requestCode == REQUEST_CODE_SCAN && resultCode == RESULT_OK) {

            if  (data != null) {

                //  return text
                String content = data.getStringExtra(DECODED_CONTENT_KEY);

                //  return bitmap
                Bitmap bitmap = data.getParcelableExtra(DECODED_BITMAP_KEY);

                tv_scanResult.setText("Scanned info: " + content);

                targetBluetoothMac = content;
            }
        }
    }
}