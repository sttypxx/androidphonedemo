/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.usw.bluetoothsdk.bluetoothgatt;

import java.util.HashMap;
import java.util.UUID;

/**
 * This class includes a small subset of standard GATT attributes for demonstration purposes.
 */
public class SampleGattAttributes {

    private static HashMap<String, String> attributes = new HashMap();

    //  Service
    /*  Current Wifi Config Service UUID */
    public static UUID WIFI_CONFIG_SERVICE          =   UUID.fromString("00001805-0000-1000-8000-00805f9b34fb");

    /* Mandatory Current Config Information Characteristic -- READ */
    public static UUID CURRENT_WIFI_CONFIG          =   UUID.fromString("00002a2c-0000-1000-8000-00805f9b34fb");

    /* Mandatory Current Config Information Characteristic -- WRITE */
    public static UUID CLIENT_WIFI_CONFIG           =   UUID.fromString("00002a2d-0000-1000-8000-00805f9b34fb");

    /* Mandatory Notify Information Characteristic -- WRITE */
    public static UUID NOTIFY_WIFI_CONFIG           =   UUID.fromString("00002a2e-0000-1000-8000-00805f9b34fb");

    /* Mandatory Client Characteristic Config Descriptor */
    public static UUID CLIENT_CONFIG_DESCRIPTOR     =   UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    public static String CURRENT_WIFI_MEASUREMENT   =   "00002a2c-0000-1000-8000-00805f9b34fb";

    static
    {
        //  Sample Services.
        attributes.put("00001805-0000-1000-8000-00805f9b34fb", "Wifi Config Service");

        //  Sample Characteristics.
        attributes.put("00002a29-0000-1000-8000-00805f9b34fb",  "Manufacturer Name String");
        attributes.put("00002a2c-0000-1000-8000-00805f9b34fb",  "Current Wifi Config");
        attributes.put("00002a2d-0000-1000-8000-00805f9b34fb",  "New Wifi Config");
        attributes.put("00002a2e-0000-1000-8000-00805f9b34fb",  "Notify Config");
    }

    /**
     *
     * @param uuid
     * @param defaultName
     * @return
     */
    public static String lookup(String uuid, String defaultName) {

        String  name = attributes.get(uuid);

        return  (name == null) ? defaultName : name;
    }

    /**
     *
     * @param uuid
     * @return
     */
    public static boolean containsUuid(String uuid)
    {
        String  name = attributes.get(uuid);

        return  (name == null) ? false : true;
    }
}
