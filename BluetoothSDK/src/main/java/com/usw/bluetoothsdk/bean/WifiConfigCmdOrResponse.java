package com.usw.bluetoothsdk.bean;

import java.io.Serializable;

public class WifiConfigCmdOrResponse implements Serializable {

    private String      cmd;
    private String      reserved;
    private WifiConfig  data;

    //  cmd
    public String getCmd() {

        return cmd;
    }

    public void setCmd(String cmd) {

        this.cmd = cmd;
    }

    //  data
    public WifiConfig getData() {

        return data;
    }

    public void setData(WifiConfig data) {

        this.data = data;
    }

    //  reserved
    public String getReserved() {

        return  reserved;
    }

    public void setReserved(String reserved) {

        this.reserved = reserved;
    }
}
