package com.usw.bluetoothsdk.bean;

public interface WifiConfigCallback {

    void onReadCallback(int code, WifiConfig config);

    void onWriteCallback(int code);
}
