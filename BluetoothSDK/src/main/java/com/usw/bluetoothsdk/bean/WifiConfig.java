package com.usw.bluetoothsdk.bean;

import java.io.Serializable;

public class WifiConfig implements Serializable {

    private String  ssid;
    private String  password;

    public WifiConfig()
    {
        ssid = "";
        password = "";
    }

    //  ssid
    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    //  password
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /***
     *
     * @return
     */
    @Override
    public String toString()
    {
        return  ("ssid = " + ssid + ",password = " + password);
    }
}
