package com.inuker.bluetooth.library.sdk.channel;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.inuker.bluetooth.library.channel.Channel;
import com.inuker.bluetooth.library.channel.ChannelCallback;
import com.inuker.bluetooth.library.sdk.bean.IDataRecvListener;
import com.inuker.bluetooth.library.sdk.bluetoothgatt.SampleGattAttributes;

import java.util.List;
import java.util.UUID;

/**
 *
 */
public class ClientChannel extends Channel {

    private static final String TAG = ClientChannel.class.getSimpleName();

    public final static String ACTION_DATA_AVAILABLE =
            "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";

    public final static String EXTRA_DATA =
            "com.example.bluetooth.le.EXTRA_DATA";

    private BluetoothGattService            wifiConfigService;
    private BluetoothGattCharacteristic     wifiConfigReadChara;
    private BluetoothGattCharacteristic     wifiConfigWriteChara;
    private Context                         context;
    private BluetoothGatt                   mGatt;
    private ChannelCallback                 channelCallback;

    private IDataRecvListener               dataRecvListener;

    /**
     *
     * @param gatt
     */
    public void setupGatt(Context context, BluetoothGatt gatt)
    {
        this.context =  context;
        this.mGatt   =  gatt;
    }

    /**
     *
     * @param
     */
    public void setupCharacteristic()
    {
        Log.i(TAG, "setupCharacteristic(), mGatt = " + mGatt);

        wifiConfigService = mGatt.getService(SampleGattAttributes.WIFI_CONFIG_SERVICE);
        if  (wifiConfigService == null) {

            Log.e(TAG, "can not get WIFI_CONFIG_SERVICE");

            return;
        }

        wifiConfigReadChara     =   wifiConfigService.getCharacteristic(SampleGattAttributes.CURRENT_WIFI_CONFIG);
        if  (wifiConfigReadChara == null) {

            Log.e(TAG, "can not get CURRENT_WIFI_CONFIG");

            return;
        }

        wifiConfigWriteChara    =   wifiConfigService.getCharacteristic(SampleGattAttributes.CLIENT_WIFI_CONFIG);
        if  (wifiConfigWriteChara == null) {

            Log.e(TAG, "can not get CLIENT_WIFI_CONFIG");

            return;
        }
    }

    /**
     *
     * @param bytes
     * @param callback
     */
    @Override
    public void write(byte[] bytes, ChannelCallback callback) {

        Log.i(TAG, "ClientChannel.write, bytes.size = " + bytes.length + ", wifiConfigWriteChara= " + wifiConfigWriteChara.getUuid().toString());

        if  (wifiConfigWriteChara != null) {

            wifiConfigWriteChara.setValue(bytes);
            wifiConfigWriteChara.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
            mGatt.writeCharacteristic(wifiConfigWriteChara);

            setChannelCallback(callback);
        }
    }

    /**
     *
     * @param bytes
     */
    @Override
    public void onRecv(byte[] bytes) {

        Log.i(TAG, "ClientChannel.onRecv(), bytes size = " + bytes.length);

        if  (dataRecvListener != null) {

            dataRecvListener.onDataRecv(bytes);
        }
    }

    /**
     *
     * @param service
     * @param characteristicUUID
     * @return
     */
    private BluetoothGattCharacteristic findNotifyCharacteristic(BluetoothGattService service, UUID characteristicUUID) {

        BluetoothGattCharacteristic characteristic = null;
        List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
        for (BluetoothGattCharacteristic c : characteristics) {

            if  ((c.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0
                    && characteristicUUID.equals(c.getUuid())) {

                characteristic = c;
                break;
            }
        }

        if  (characteristic != null)
            return  characteristic;

        for (BluetoothGattCharacteristic c : characteristics) {

            if  ((c.getProperties() & BluetoothGattCharacteristic.PROPERTY_INDICATE) != 0
                    && characteristicUUID.equals(c.getUuid())) {

                characteristic = c;
                break;
            }
        }

        return  characteristic;
    }

    public ChannelCallback getChannelCallback() {
        return channelCallback;
    }

    public void setChannelCallback(ChannelCallback channelCallback) {

        this.channelCallback = channelCallback;
    }

    //  dataRecvListener
    public IDataRecvListener getDataRecvListener() {

        return  dataRecvListener;
    }

    public void setDataRecvListener(IDataRecvListener dataRecvListener) {

        this.dataRecvListener = dataRecvListener;
    }
}
