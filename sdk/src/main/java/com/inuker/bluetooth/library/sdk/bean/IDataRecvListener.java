package com.inuker.bluetooth.library.sdk.bean;

public interface IDataRecvListener {

    void onDataRecv(byte[] bytes);
}
