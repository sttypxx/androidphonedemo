package com.inuker.bluetooth.library.sdk.bean;

public interface WifiConfigCallback {

    void onReadCallback(int code, WifiConfig config);

    void onWriteCallback(int code);
}
