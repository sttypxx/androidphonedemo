/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inuker.bluetooth.library.sdk.bluetoothgatt;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import com.google.gson.Gson;
import com.inuker.bluetooth.library.channel.ChannelCallback;
import com.inuker.bluetooth.library.channel.Code;
import com.inuker.bluetooth.library.sdk.bean.IDataRecvListener;
import com.inuker.bluetooth.library.sdk.bean.WifiConfig;
import com.inuker.bluetooth.library.sdk.bean.WifiConfigCallback;
import com.inuker.bluetooth.library.sdk.bean.WifiConfigCmdOrResponse;
import com.inuker.bluetooth.library.sdk.channel.ClientChannel;

import java.util.List;
import java.util.UUID;

import static android.bluetooth.BluetoothDevice.TRANSPORT_LE;

/**
 * Service for managing connection and data communication with a GATT server hosted on a
 * given Bluetooth LE device.
 */
public class BluetoothLeService extends Service implements IDataRecvListener
{

    private final static String TAG = BluetoothLeService.class.getSimpleName();

    private BluetoothManager    mBluetoothManager;
    private BluetoothAdapter    mBluetoothAdapter;
    private String              mBluetoothDeviceAddress;
    private BluetoothGatt       mBluetoothGatt;
    private ClientChannel       mClientChannel;

    private int mConnectionState = STATE_DISCONNECTED;

    private static final int STATE_DISCONNECTED =   0;
    private static final int STATE_CONNECTING   =   1;
    private static final int STATE_CONNECTED    =   2;

    public final static String ACTION_GATT_CONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_CONNECTED";

    public final static String ACTION_GATT_DISCONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";

    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";

    public final static String ACTION_DATA_AVAILABLE =
            "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";

    public final static String EXTRA_DATA =
            "com.example.bluetooth.le.EXTRA_DATA";

    public final static UUID UUID_CURRENT_WIFI_MEASUREMENT   =
            UUID.fromString(SampleGattAttributes.CURRENT_WIFI_MEASUREMENT);

    //  Implements callback methods for GATT events that the app cares about.  For example,
    //  connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {

            String  intentAction;

            Log.i(TAG, "onConnectionStateChange, newState = " + newState);

            if  (status == 0) {

                //  status equal to 0

                if  (newState == BluetoothProfile.STATE_CONNECTED) {

                    intentAction = ACTION_GATT_CONNECTED;
                    mConnectionState = STATE_CONNECTED;
                    broadcastUpdate(intentAction);
                    Log.i(TAG, "Connected to GATT server.");

                    //  Attempts to discover services after successful connection.
                    Log.i(TAG, "Attempting to start service discovery:" +
                            mBluetoothGatt.discoverServices());

                    startChannel(gatt);

                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {

                    intentAction = ACTION_GATT_DISCONNECTED;
                    mConnectionState = STATE_DISCONNECTED;
                    Log.i(TAG, "Disconnected from GATT server.");
                    broadcastUpdate(intentAction);

                    closeChannel();
                }
            }
            else
            {
                //  status not equal to 0
                Log.e(TAG, "status = " + status);

                mBluetoothGatt.disconnect();
                mBluetoothGatt.close();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {

            Log.i(TAG, "onServicesDiscovered, status = " + status);

            if  (status == BluetoothGatt.GATT_SUCCESS) {

                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);

                if  (mClientChannel != null) {

                    mClientChannel.setupCharacteristic();
                }

                //  enable Notification
                enableNotification(mBluetoothGatt, SampleGattAttributes.WIFI_CONFIG_SERVICE,
                        SampleGattAttributes.NOTIFY_WIFI_CONFIG);
            } else {

                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        /**
         *
         * @param gatt
         * @param characteristic
         * @param status
         */
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {

            Log.i(TAG, String.format("onCharacteristicRead: status = %d", status));
        }

        /**
         *
         *  onCharacteristicWrite
         * */
        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

            super.onCharacteristicWrite(gatt, characteristic, status);

            Log.e(TAG, "onCharacteristicWrite(), characteristic = " + characteristic.getUuid().toString() +
                    ",status = " + status);

            if  (mClientChannel != null) {

                ChannelCallback channelCallback;

                channelCallback = mClientChannel.getChannelCallback();

                if  (channelCallback != null) {

                    Log.i(TAG, "onCharacteristicWrite(), call ChannelCallback.onCallback " + ",status = " + status);

                    channelCallback.onCallback(status == BluetoothGatt.GATT_SUCCESS? Code.SUCCESS : Code.FAIL);

                    mClientChannel.setChannelCallback(null);
                }
            }
            else
            {
                Log.i(TAG, "mClientChannel is null");
            }
        }

        /**
         *  onCharacteristicChanged
         *  @param gatt
         *  @param characteristic
         */
        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {

            Log.i(TAG, "Client.onCharacteristicChanged(), notify characteristic.UUID = " + characteristic.getUuid().toString());
            Log.i(TAG, "Client.onCharacteristicChanged(), data size = " + characteristic.getValue().length);

            byte[]  data_received;

            data_received = characteristic.getValue();

            if  (mClientChannel != null)
            {
                Log.i(TAG, "Client.onRead() called, data_received size = " + data_received.length);

                mClientChannel.onRead(data_received);
            }
        }
    };

    /**
     *  startChannel
     *  @param gatt
     */
    private void startChannel(BluetoothGatt gatt)
    {
        mClientChannel = new ClientChannel();

        mClientChannel.setupGatt(BluetoothLeService.this, gatt);
    }

    /**
     *
     */
    private void closeChannel()
    {

    }

    /**
     *
     * @param action
     */
    private void broadcastUpdate(final String action) {

        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    /**
     *
     * @param action
     * @param characteristic
     */
    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {

        final   Intent intent = new Intent(action);

        // This is special handling for the Heart Rate Measurement profile.  Data parsing is
        // carried out as per profile specifications:
        // http://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.heart_rate_measurement.xml

//        if  (UUID_CURRENT_WIFI_MEASUREMENT.equals(characteristic.getUuid()))
//        {
//            String  strWifiConfig;
//
//            strWifiConfig = IndataHandler.getInstance().getMsgReceived();
//
//            //  Log.i(TAG, "onCharacteristicRead data string received = " + strWifiConfig);
//            //
//            //  final byte[] data = characteristic.getValue();
//            //
//            //  if  (data != null && data.length > 0) {
//            //
//            //    strWifiConfig = new String(data);
//            //
//
//            intent.putExtra(EXTRA_DATA, strWifiConfig);
//
//            //  }
//        }
//        else {
//
//            // For all other profiles, writes the data formatted in HEX.
//            final byte[] data = characteristic.getValue();
//            if  (data != null && data.length > 0) {
//
//                final StringBuilder stringBuilder = new StringBuilder(data.length);
//                for(byte byteChar : data)
//                    stringBuilder.append(String.format("%02X ", byteChar));
//
//                intent.putExtra(EXTRA_DATA, new String(data) + "\n" + stringBuilder.toString());
//            }
//        }
//
//        sendBroadcast(intent);
    }

    /**
     *
     * @param gatt
     * @param serviceUUID
     * @param characteristicUUID
     * @return
     */
    public boolean enableNotification(BluetoothGatt gatt, UUID serviceUUID, UUID characteristicUUID) {

        boolean     success = false;

        BluetoothGattService service = gatt.getService(serviceUUID);

        if  (service != null) {

            BluetoothGattCharacteristic characteristic = findNotifyCharacteristic(service, characteristicUUID);

            Log.i(TAG, "enableNotification.findNotifyCharacteristic(), characteristic = " + characteristic);

            if  (characteristic != null) {

                success = gatt.setCharacteristicNotification(characteristic, true);

                Log.i(TAG, "gatt.setCharacteristicNotification(), result = " + success);

                if  (success) {

                    // 来源：http://stackoverflow.com/questions/38045294/oncharacteristicchanged-not-called-with-ble
                    for (BluetoothGattDescriptor dp: characteristic.getDescriptors()) {
                        if (dp != null) {

                            if  ((characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0) {

                                dp.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                            } else if ((characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_INDICATE) != 0) {

                                dp.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
                            }

                            gatt.writeDescriptor(dp);
                        }
                    }
                }
            }
        }

        return  success;
    }

    /**
     *
     * @param service
     * @param characteristicUUID
     * @return
     */
    private BluetoothGattCharacteristic findNotifyCharacteristic(BluetoothGattService service, UUID characteristicUUID) {

        BluetoothGattCharacteristic characteristic = null;
        List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();

        for (BluetoothGattCharacteristic c : characteristics) {

            if  ((c.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0
                    && characteristicUUID.equals(c.getUuid())) {

                characteristic = c;
                break;
            }
        }

        if  (characteristic != null)
            return  characteristic;

        for (BluetoothGattCharacteristic c : characteristics) {

            if  ((c.getProperties() & BluetoothGattCharacteristic.PROPERTY_INDICATE) != 0
                    && characteristicUUID.equals(c.getUuid())) {

                characteristic = c;
                break;
            }
        }

        return  characteristic;
    }

    /**
     *  On Data Received
     *  @param bytes
     */
    @Override
    public void onDataRecv(byte[] bytes) {

        String msg = new String(bytes);

        Log.i(TAG, "BluetoothLeService.onDataRecv(), msg = " + msg);

        broadcastUpdateData(ACTION_DATA_AVAILABLE, msg);
    }

    /**
     *
     * @param action
     */
    private void broadcastUpdateData(final String action, String msgData) {

        final Intent intent = new Intent(action);
        intent.putExtra(EXTRA_DATA, msgData);

        sendBroadcast(intent);
    }

    /**
     *
     */
    public class LocalBinder extends Binder {

        public  BluetoothLeService getService() {
            return  BluetoothLeService.this;
        }
    }

    /**
     *
     * @param intent
     * @return
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /**
     *
     * @param intent
     * @return
     */
    @Override
    public boolean onUnbind(Intent intent) {

        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        close();

        return  super.onUnbind(intent);
    }

    private final IBinder mBinder = new LocalBinder();

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {

        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if  (mBluetoothManager == null) {

            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if  (mBluetoothManager == null) {

                Log.e(TAG, "Unable to initialize BluetoothManager.");

                return  false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if  (mBluetoothAdapter == null) {

            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");

            return  false;
        }

        return  true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     *
     * @return Return true if the connection is initiated successfully. The connection result
     *         is reported asynchronously through the
     *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     *         callback.
     */
    public boolean connect(final String address) {

        if  (mBluetoothAdapter == null || address == null) {

            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");

            return  false;
        }

        //  Previously connected device.  Try to reconnect.
        if  (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {

            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");

            mBluetoothGatt.disconnect();

            mBluetoothGatt.close();

            mBluetoothGatt = null;
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if  (device == null) {

            Log.w(TAG, "Device not found.  Unable to connect.");

            return  false;
        }

        int deviceType = device.getType();

        Log.i(TAG, "Build.VERSION.SDK_INT = " + Build.VERSION.SDK_INT);
        Log.i(TAG, "Remote Device Type    = " + deviceType);

        if  (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            mBluetoothGatt = device.connectGatt(this,
                    false, mGattCallback, TRANSPORT_LE);  //

        } else {

            mBluetoothGatt = device.connectGatt(this,
                    false, mGattCallback);
        }

        //  We want to directly connect to the device, so we are setting the autoConnect
        //  parameter to false.

        Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;

        return  true;
    }

    /**
     *  Disconnects an existing connection or cancel a pending connection. The disconnection result
     *  is reported asynchronously through the
     *  {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     *  callback.
     */
    public void disconnect() {

        if  (mBluetoothAdapter == null || mBluetoothGatt == null) {

            Log.w(TAG, "BluetoothAdapter not initialized");

            return;
        }

        mBluetoothGatt.disconnect();
    }

    /**
     *  close
     *  After using a given BLE device, the app must call this method to ensure resources are
     *  released properly.
     */
    public void close() {

        if  (mBluetoothGatt == null) {

            return;
        }

        mBluetoothGatt.close();

        mBluetoothGatt = null;
    }

    /**
     *  Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     *  asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     *  callback.
     *
     *  @param characteristic The characteristic to read from.
     */
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {

        if  (mBluetoothAdapter == null || mBluetoothGatt == null) {

            Log.w(TAG, "BluetoothAdapter not initialized");

            return;
        }

        mBluetoothGatt.readCharacteristic(characteristic);
    }

    private BluetoothGattService            wifiConfigService;
    private BluetoothGattCharacteristic     wifiConfigReadChara;
    private BluetoothGattCharacteristic     wifiConfigWriteChara;
    private BluetoothGattCharacteristic     wifiNotifyChara;

    /**
     *  readCurrentWifiConfig
     *  @return
     */
    public void readCurrentWifiConfig(WifiConfigCallback callback)
    {
        Gson    gson = new Gson();
        String  strWifiConfigCmdOrResponse;

        WifiConfig  wifiConfig;
        WifiConfigCmdOrResponse wifiConfigCmdOrResponse;

        wifiConfig = new WifiConfig();

        wifiConfigCmdOrResponse = new WifiConfigCmdOrResponse();
        wifiConfigCmdOrResponse.setCmd("Get");
        wifiConfigCmdOrResponse.setReserved("");

        this.mCallback  =   callback;

        wifiConfigService = mBluetoothGatt.getService(SampleGattAttributes.WIFI_CONFIG_SERVICE);

        if  (wifiConfigService == null) {

            Log.e(TAG, "can not get WIFI_CONFIG_SERVICE");

            return;
        }

        wifiConfigReadChara     =   wifiConfigService.getCharacteristic(SampleGattAttributes.CURRENT_WIFI_CONFIG);
        if  (wifiConfigReadChara == null) {

            Log.e(TAG, "can not get CURRENT_WIFI_CONFIG");

            return;
        }

        //  Do the write operation
        wifiConfigCmdOrResponse.setData(wifiConfig);

        //  Do Packet Adjust(let the crc code in one separate packet)
        doPacketAdjust(wifiConfigCmdOrResponse);

        strWifiConfigCmdOrResponse = gson.toJson(wifiConfigCmdOrResponse);

        mClientChannel.send(strWifiConfigCmdOrResponse.getBytes(), new ChannelCallback() {

            @Override
            public void onCallback(int code) {

                Log.i(TAG, "ClientChannel.onCallback, code = " + code);
            }
        });
    }

    private WifiConfigCallback  mCallback;

    /**
     *  writeWifiConfig
     *  @param wifiConfig
     *  @return
     */
    public int writeWifiConfig(WifiConfig wifiConfig, WifiConfigCallback callback)
    {
        Gson    gson = new Gson();
        String  strWifiConfigCmdOrResponse;

        WifiConfigCmdOrResponse wifiConfigCmdOrResponse;

        this.mCallback = callback;

        wifiConfigService = mBluetoothGatt.getService(SampleGattAttributes.WIFI_CONFIG_SERVICE);

        if  (wifiConfigService == null) {

            Log.i(TAG, "wifiConfigService is NULL");

            return  0;
        }

        wifiConfigWriteChara     =   wifiConfigService.getCharacteristic(SampleGattAttributes.CLIENT_WIFI_CONFIG);
        if  (wifiConfigWriteChara == null) {

            Log.i(TAG, "wifiConfigWriteChara is NULL");

            return  0;
        }

        int test = wifiConfigWriteChara.getProperties();                                      //Get the properties of the characteristic

        if  ((test & BluetoothGattCharacteristic.PROPERTY_WRITE) == 0 && (test & BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE) == 0) { //Check that the property is writable

            Log.i(TAG, "Do not have write permission");

            return  0;
        }

        //  Do the write operation
        wifiConfigCmdOrResponse = new WifiConfigCmdOrResponse();

        wifiConfigCmdOrResponse.setCmd("Set");
        wifiConfigCmdOrResponse.setReserved("");
        wifiConfigCmdOrResponse.setData(wifiConfig);

        doPacketAdjust(wifiConfigCmdOrResponse);

        strWifiConfigCmdOrResponse = gson.toJson(wifiConfigCmdOrResponse);

        mClientChannel.send(strWifiConfigCmdOrResponse.getBytes(), new ChannelCallback() {

            @Override
            public void onCallback(int code) {

                Log.i(TAG, "ClientChannel.onCallback, code = " + code);
            }
        });

        return  0;
    }

    /**
     *
     * @param wifiConfigCmdOrResponse
     */
    private void doPacketAdjust(WifiConfigCmdOrResponse wifiConfigCmdOrResponse)
    {
        byte[]	    wifiConfigBuffer;
        int 	    bufferSize;
        String  	strWifiConfigCmdOrResponse;
        Gson    	gson = new Gson();

        strWifiConfigCmdOrResponse = gson.toJson(wifiConfigCmdOrResponse);
        wifiConfigBuffer = strWifiConfigCmdOrResponse.getBytes();

        bufferSize = wifiConfigBuffer.length;

        if  ((bufferSize + 2) % 18 == 1) {
            wifiConfigCmdOrResponse.setReserved("a"); //
        }
    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled If true, enable notification.  False otherwise.
     */
    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic,
                                              boolean enabled) {

        if  (mBluetoothAdapter == null || mBluetoothGatt == null) {

            Log.w(TAG, "BluetoothAdapter not initialized");

            return;
        }

        mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);

        //  This is specific to Heart Rate Measurement.
        if  (UUID_CURRENT_WIFI_MEASUREMENT.equals(characteristic.getUuid())) {

            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
                    SampleGattAttributes.CLIENT_CONFIG_DESCRIPTOR);

            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            mBluetoothGatt.writeDescriptor(descriptor);
        }
    }

    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
    public List<BluetoothGattService> getSupportedGattServices() {

        if  (mBluetoothGatt == null) return null;

        return  mBluetoothGatt.getServices();
    }
}
