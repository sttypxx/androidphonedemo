package com.inuker.bluetooth.library.sdk.bean;

public class BLE {

    //--------------------------------------------------------
    //  packet format
    //  byte[0] =   header byte, that is 0x55
    //  byte[1] =   packet size
    //  byte[2] =   packet flag, continue or last packet
    //--------------------------------------------------------

    //  Default MTU size for BLE
    public static final int  DEFAULT_BYTES_VIA_BLE              =   20;

    //  packet header
    public static final byte HEADER_BYTE                        =   0x55;

    //  packet size
    public static final int  DEFAULT_BYTES_IN_CONTINUE_PACKET   =   17;
    public static final int  INITIAL_MESSAGE_PACKET_LENGTH      =   3;

    //  packet flag
    public static final byte CONTINUE_MESSAGE_PACKET            =   (byte)0x01;
    public static final byte LAST_MESSAGE_PACKET                =   (byte)0x00;

    public static final int  DATA_START_INDEX                   =   3;
    public static final int  HEADER_EXTRA_DATA_SIZE             =   3;
}
